<?php
class Automatify {

	protected $config;
	protected $view_path;
	protected $return_method;
	
	protected $query;
	protected $search_columns;
	protected $search_query;
	protected $offset;
	protected $sort_types = array('asc', 'desc');
	
	protected $pagination;
	
	public $current_items;
		
	public function __construct($query, $search_columns, $order = null, $method = null) 
	{	
		$this->config = Kohana::$config->load('automatify');
		$this->view_path = $this->config->get('view_path').Request::initial()->controller().'/_list';
		$this->return_method = $method != null ? $method : $this->config->get('return_as');
		
		$per_page = Request::initial()->query('perpage');
		$user_order = strtolower(Request::initial()->query('order'));
		$user_order_type = strtolower(Request::initial()->query('order_type'));
		
		$this->query = $query;
		$this->search_columns = $search_columns;
		
		if(empty($user_order) OR empty($user_order_type) 
		OR !in_array($user_order_type, $this->sort_types)) 
		{	
			$this->order = empty($order) ? $this->config->get('default_order') : $order;
		}
		else 
		{
			$this->order = array($user_order => $user_order_type);
		}
		
		$this->search_query = Request::current()->query('search_query');
		$this->per_page = $this->get_per_page();
		$this->set_search_conditions();
		$this->configure_pagination();
		$this->get_rows();
		
		if(Request::initial()->is_ajax())
		{
			$this->ajax();
		}
	}
	
	protected function set_search_conditions()
	{
		if(!empty($this->search_query))
		{
			foreach($this->search_columns as $column)
			{
				$this->query->or_where($column, 'like', '%'.$this->search_query.'%');
			}
		}
	}
	
	protected function count_total()
	{
		$query = clone $this->query;
		$result = $query->as_object()->execute();
		return count($result);	
	}
	
	protected function configure_pagination()
	{
		$config =  array(
			'total_items'    	=> $this->count_total(),
			'items_per_page' 	=> $this->per_page
		);
		$pagination = Pagination::factory($config);
		$this->offset = $pagination->offset;
		$this->pagination = $pagination;
	}
	
	protected function get_rows()
	{
		$this->query->limit($this->per_page)->offset($this->offset);
		
		foreach($this->order as $by => $order)
		{
			$this->query->order_by($by, $order);
		}
		
		$query = clone $this->query;
		$method = $this->return_method;
		$this->current_items = $query->$method()->execute();
	}
	
	public function get()
	{
		return $this->current_items;
	}
	
	public function __toString()
	{
		return $this->pagination->__toString();
	}
	
	protected function ajax()
	{
		$response = array();
		$response['data'] = (string) View::factory($this->view_path)
			->set('items', $this->current_items);
		
		// change line below...
		$response['pagination'] = $this->pagination->__toString();
		
		$order_table = strtolower(key(current($this->order)));
		$order_type  = strtolower(current($this->order));
		
		$response['order'] = Request::initial()->current()
		->uri().URL::query(array(
			'order' => $order_table, 
			'order_type' => $this->sort_types[(int) !array_search($order_type, $this->sort_types)]
		));
		
		// lines below need a little change...
		echo json_encode($response);
		die();
	}
	
	protected function get_per_page()
	{
		$per_page = Request::initial()->query('per_page');
		if(in_array($per_page, 	$this->config->get('per_page_allowed')))
		{
			return $per_page;
		}
		else {
			return $this->config->get('per_page');
		}
	}
	
	public function sort_header($name, $column)
	{
		$order_type = strtolower(current($this->order));
		$order_to_show = $this->sort_types[(int) !array_search($order_type, $this->sort_types)];
		$url_query = '?order='.$column.'&order_type='.$order_to_show;
		return html::anchor(Request::initial()->current()->uri().$url_query, 
			$name, array('class' => 'sortHeader'));
	}
}
?>
