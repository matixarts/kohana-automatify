<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'default_order' => array('id' => 'desc'),
	'view_path'		=> '/',
	'per_page'		=> 10, 
	'per_page_allowed' => array(10, 20, 50),
	'return_as' => 'as_object', // as_object or as_assoc
);
